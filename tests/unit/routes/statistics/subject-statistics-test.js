import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';

module('Unit | Route | statistics/subject-statistics', function(hooks) {
  setupTest(hooks);

  test('it exists', function(assert) {
    let route = this.owner.lookup('route:statistics/subject-statistics');
    assert.ok(route);
  });
});
