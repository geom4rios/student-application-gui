import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';

module('Unit | Route | statistics/student-statistics', function(hooks) {
  setupTest(hooks);

  test('it exists', function(assert) {
    let route = this.owner.lookup('route:statistics/student-statistics');
    assert.ok(route);
  });
});
