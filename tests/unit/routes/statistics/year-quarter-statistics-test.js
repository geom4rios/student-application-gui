import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';

module('Unit | Route | statistics/year-quarter-statistics', function(hooks) {
  setupTest(hooks);

  test('it exists', function(assert) {
    let route = this.owner.lookup('route:statistics/year-quarter-statistics');
    assert.ok(route);
  });
});
