import Application from 'student-application-gui/app';
import config from 'student-application-gui/config/environment';
import { setApplication } from '@ember/test-helpers';
import { start } from 'ember-qunit';

setApplication(Application.create(config.APP));

start();
