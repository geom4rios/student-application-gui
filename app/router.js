import EmberRouter from '@ember/routing/router';
import config from 'student-application-gui/config/environment';

export default class Router extends EmberRouter {
  location = config.locationType;
  rootURL = config.rootURL;
}

Router.map(function() {
  this.route('data-entry');
  this.route('student', {path: '/student/:student_id'});


  this.route('statistics', function() {
    this.route('student-statistics', function() {
      this.route('student-statistic', { path: '/:student_id' });
      this.route('index', { path: '/' });
    })
    this.route('subject-statistics', function() {
      this.route('subject-statistic', { path: '/:subject_id'});
      this.route('index', { path: '/'});
    });
    this.route('year-quarter-statistics', function() {
      this.route('year-quarter-statistic', { path: '/:year_quarter_id'});
      this.route('index', { path: '/'});
    });
  });
});
