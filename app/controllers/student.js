import Controller from '@ember/controller';
import { action } from '@ember/object';
import {tracked} from "@glimmer/tracking";

export default class StudentController extends Controller {

  @tracked
  gradeInformation = {
    "year": '',
    "quarter": '',
    "math": '',
    "computer": '',
    "literature": ''
  }

  @tracked
  openModal = false;

  @action
  toggleModal() {
    this.toggleProperty('openModal');
  }

  @action
  async submit(student_id) {
    const requestBody = {studentId: student_id, ...this.gradeInformation}
    console.log(JSON.stringify(requestBody));
    const response = await fetch('http://localhost:8080/grade', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(requestBody)
    })
    const createdGrade = await response.json();

    if (response.status === 200) {
      this.gradeInformation = {
        "year": '',
        "quarter": '',
        "math": '',
        "computer": '',
        "literature": ''
      };

      alert('Successfully added grade for student!');
    } else {
      alert('Oops! something went wrong when trying to add grade for student!');
    }
  }

}
