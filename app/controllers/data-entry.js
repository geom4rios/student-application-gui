import Controller from '@ember/controller';
import { action } from '@ember/object';
import {tracked} from "@glimmer/tracking";

export default class DataEntryController extends Controller {

  @tracked
  student = {
    "name": '',
    "username": '',
    "class": '',
    "dateOfBirth": ''
  }

  @tracked
  openModal = false;

  @action
  toggleModal() {
    this.toggleProperty('openModal');
  }

  @action
  async submit() {
    const response = await fetch('http://localhost:8080/student', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(this.student)
    })
    const createdStudent = await response.json();

    if (response.status === 200) {
      this.student = {
        "name": '',
        "username": '',
        "class": '',
        "dateOfBirth": ''
      };

      alert('Successfully created new student!');
    } else {
      alert('Oops! something went wrong when trying to create the new student!')
    }
  }
}
