import Route from '@ember/routing/route';

export default class StatisticsStudentStatisticsStudentStatisticRoute extends Route {
  async model(params) {
    const response = await fetch('http://localhost:8080/statistics/studentperquarter/' + params.student_id,{
      method: 'GET',
      headers: {
        'Content-Type': 'application/json'
      }
    });
    const studentaverage = await response.json();

    let labels = [];
    let data = [];
    studentaverage.quarters.forEach(quarter => {
      labels.push(quarter.quarter);
      data.push(quarter.average);
    })

    return {
      username: studentaverage.username,
      data: {
        labels: labels,
        datasets: [{
          label: studentaverage.username + '\'s average per quarter',
          backgroundColor: 'rgb(255, 99, 132)',
          borderColor: 'rgb(255, 99, 132)',
          data: data
        }]
      }
    }
  }
}
