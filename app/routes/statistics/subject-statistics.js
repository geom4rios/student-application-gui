import Route from '@ember/routing/route';

export default class StatisticsSubjectStatisticsRoute extends Route {
  model() {
    return {
      subjects: ["Math", "Computer", "Literature"]
    };
  }
}
