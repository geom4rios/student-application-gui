import Route from '@ember/routing/route';

export default class StatisticsYearQuarterStatisticsRoute extends Route {
  async model() {
    const response = await fetch('http://localhost:8080/available-quarters',{
      method: 'GET',
      headers: {
        'Content-Type': 'application/json'
      }
    });
    return await response.json();
  }
}
