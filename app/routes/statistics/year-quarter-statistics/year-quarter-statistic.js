import Route from '@ember/routing/route';

export default class StatisticsYearQuarterStatisticsYearQuarterStatisticRoute extends Route {
  async model(params) {
    let yearQuarter = params.year_quarter_id;
    let year = yearQuarter.split('-')[0];
    let quarter = yearQuarter.split('-')[1];

    const response = await fetch('http://localhost:8080/statistics/yearquarteraverage/' + year + '/' + quarter,{
      method: 'GET',
      headers: {
        'Content-Type': 'application/json'
      }
    });
    const year_quarter_average = await response.json();

    let data = [];
    data.push(year_quarter_average['math_average']);
    data.push(year_quarter_average['computer_average']);
    data.push(year_quarter_average['literature_average']);

    return {
      yearQuarter: yearQuarter,
      data: {
        labels: ['Math', 'Computer', 'Literature'],
        datasets: [{
          label: 'Subject average for yearly quarter: ' + yearQuarter,
          backgroundColor: 'rgb(255, 99, 132)',
          borderColor: 'rgb(255, 99, 132)',
          data: data
        }]
      }
    }
  }
}
