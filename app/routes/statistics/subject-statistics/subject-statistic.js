import Route from '@ember/routing/route';

export default class StatisticsSubjectStatisticsSubjectStatisticRoute extends Route {
  async model(params) {

    const response = await fetch('http://localhost:8080/statistics/subjectperquarter/' + params.subject_id,{
      method: 'GET',
      headers: {
        'Content-Type': 'application/json'
      }
    });
    const subject_average = await response.json();

    let labels = [];
    let data = [];
    subject_average.quarters.forEach(quarter => {
      labels.push(quarter.quarter);
      data.push(quarter.average);
    })

    return {
      subject: subject_average.subject,
      data: {
        labels: labels,
        datasets: [{
          label: subject_average.subject + ' average per quarter',
          backgroundColor: 'rgb(255, 99, 132)',
          borderColor: 'rgb(255, 99, 132)',
          data: data
        }]
      }
    }
  }
}
