import Route from '@ember/routing/route';

export default class DataEntryRoute extends Route {
  async model(params) {
    const response = await fetch('http://localhost:8080/student/' + params.student_id,{
      method: 'GET',
      headers: {
        'Content-Type': 'application/json'
      }
    });
    return await response.json();
  }
}
