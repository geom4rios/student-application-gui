import Route from '@ember/routing/route';

export default class DataEntryRoute extends Route {
  async model() {
    const response = await fetch('http://localhost:8080/student',{
      method: 'GET',
        headers: {
        'Content-Type': 'application/json'
      }
    });
    const students = await response.json();

    return { students };
  }
}
