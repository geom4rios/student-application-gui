import Model, { attr, hasMany } from '@ember-data/model';

export default class StudentModel extends Model {
  @attr id;
  @attr('string') username;
  @attr('string') name;
  @attr('date') dateOfBirth;
  @attr('string') class;
  @hasMany('grade-information') gradeInformations;
}
