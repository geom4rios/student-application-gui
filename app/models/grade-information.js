import Model, { attr } from '@ember-data/model';

export default class GradeInformationModel extends Model {
  @attr id;
  @attr year;
  @attr quarter;
  @attr maths;
  @attr computer;
  @attr literature;
  @belongsTo('student') student;
}
